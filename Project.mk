#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Joel T. Palmer
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: Creates nameplates used by the framework
#==============================================================================

PackageName=osp.xilinx
PackagePrefix=ocpi
ProjectDependencies=ocpi.platform ocpi.assets
ComponentLibraries+=misc_comps util_comps dsp_comps comms_comps
