--==============================================================================
-- Company:     Geon Technologies, LLC
-- Author:      Joel T. Palmer
-- Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
--              Dissemination of this information or reproduction of this
--              material is strictly prohibited unless prior written
--              permission is obtained from Geon Technologies, LLC
-- Description: Zynq primtive package for the zcu111
--==============================================================================

library ieee; use IEEE.std_logic_1164.all, ieee.numeric_std.all;
library axi;
package zynq_ultra_zcu111_pkg is

constant C_M_AXI_HP_COUNT : natural := 1;
constant C_S_AXI_HP_COUNT : natural := 4;

type ps2pl_t is record
  FCLK         : std_logic_vector(3 downto 0);
  FCLKRESET_N  : std_logic;
end record ps2pl_t;
type pl2ps_t is record
  DEBUG        : std_logic_vector(31 downto 0); --     FTMT_F2P_DEBUG
end record pl2ps_t;

component zynq_ultra_zcu111_ps is
  port(ps_in        : in    pl2ps_t;
       ps_out       : out   ps2pl_t;
       m_axi_hp_in  : in    axi.zynq_ultra_m_hp.axi_s2m_array_t(0 to C_M_AXI_HP_COUNT-1);
       m_axi_hp_out : out   axi.zynq_ultra_m_hp.axi_m2s_array_t(0 to C_M_AXI_HP_COUNT-1);
       s_axi_hp_in  : in    axi.zynq_ultra_s_hp.axi_m2s_array_t(0 to C_S_AXI_HP_COUNT-1);
       s_axi_hp_out : out   axi.zynq_ultra_s_hp.axi_s2m_array_t(0 to C_S_AXI_HP_COUNT-1));
end component zynq_ultra_zcu111_ps;
end package zynq_ultra_zcu111_pkg;
